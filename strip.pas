{$mode objfpc}
program strip;
var f,g : text; s : string; out : boolean;
begin
  assign (f,'pcom.pas');  reset   (f);
  assign (g,'p6com.pas'); rewrite (g);

  out := true;
  while not eof(f) do
  begin
    readln (f,s);
    if copy (s,1,7)='{$ifdef' then out := false;

    if out and not (copy (s,1,7)='{$endif')
    then writeln (g,s);

    if copy (s,1,7)='{$endif' then out := true;
    if copy (s,1,7)='{$lse'   then out := true
  end;

  close (f); close (g)
end.